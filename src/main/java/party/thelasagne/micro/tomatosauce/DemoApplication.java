package party.thelasagne.micro.tomatosauce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {
	//testing something Mr. Tomato Man
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
